var app = require('express')();
var bodyParser = require('body-parser');
var http = require('http');
var https = require('https');
var fs = require('fs-extra');
var exec = require('child_process').exec;
var url = require("url");
var fsPath = require('fs-path');
/*********************************/

app.set('port', 8080);

var supportedExtensions = ["docx", "doc", "odt"]

var parser = bodyParser.urlencoded({ extended: false });    
app.post("/api", parser, function (req, res) {	
	if(req.body.url == undefined){res.status(400).end('{"status":0, "error":"param ' + "'url'" +' must be defined"}');  return;}
    var remotePath = url.parse(req.body.url).pathname;	
	if(!isExtensionSupported(remotePath,supportedExtensions)){
		console.log("Extension not supported");
		res.status(412).end('{"status":0, "error":"Extension not supported"}');
		return;
	}
	var dataDir = "/home/pdf-converter/data";
	var tempFileName = Date.now();	
	
    var fileToConvert = dataDir + "/in/" + tempFileName;	
	var downloadTempFile = dataDir + "/download/"+tempFileName;

    fsPath.writeFile(fileToConvert, 'content', function(err){
		
      var file = fs.createWriteStream(downloadTempFile); 
      var request = https.get(req.body.url, function(response) {                 
		
		if(response.statusCode != 200){ 
			console.log("Error. Status code: " + response.statusCode);
			res.status(response.statusCode).end('{"status":0, "error":"Status code for url param: '+response.statusCode+'"}'); 
			fs.unlink(downloadTempFile);  
			return;
		}
		
		console.log("Downloading file: "+req.body.url);
        response.pipe(file);
        response.on('end', function(err) {			
          var source = fs.createReadStream(downloadTempFile);
          var dest = fs.createWriteStream(fileToConvert);
          source.pipe(dest);

          fs.unlink(downloadTempFile);          		  
          
		  var outDirectory = dataDir+"/out/";
		  
          var convertFileCommand = 'bash /usr/bin/soffice --headless --convert-to pdf:writer_pdf_Export --outdir ' + outDirectory + ' ' + fileToConvert
          exec(convertFileCommand,function(error,stdout,stderr){
			  if(error == null){				  
				 console.log("File converted succesfully to PDF: " + outDirectory + tempFileName+".pdf");				  				 				 				 				 		 
				 
				 var filename = outDirectory+tempFileName+".pdf";
		  		 var options = {
				   	headers: {
						   'Content-Type' : 'application/pdf',						   
					   }
				 }
				
				 res.sendFile(filename, {}, function (err) {
					if (err) {
						console.log(err);
						res.status(err.status).end('{"status":0, "error":"Error while transfering the PDF file"}');
					}
					else {						
						console.log('Sent:', filename);	
						//Remove temp files
						fs.removeSync(fileToConvert);	
						fs.removeSync(filename);														
					}
				});				 		 
				  
			  }else{
				  res.status(500).end('{"status":0, "error":"File could not be converted to PDF"}');
				  console.log(error);
			  }
		  });
          
        });            
    });
    });
});
 
app.listen(app.get('port'), function(){
	console.log('Server listening on port: ', app.get('port'));
});

function isExtensionSupported(filename, supportedExtensions){
	var fileExtension = filename.split(".").pop();
	var isSupported = false;
	for(i=0;i<supportedExtensions.length;i++){
		if(supportedExtensions[i].includes(fileExtension)){
			isSupported = true;
			break;
		}
	}
	return isSupported;
}