var app = require('express')();
var bodyParser = require('body-parser');
var http = require('http');
var https = require('https');
var fs = require('fs-extra');
var AWS = require('aws-sdk');
var exec = require('child_process').exec;
var url = require("url");
var fsPath = require('fs-path');
/*********************************/

app.set('port', 8080);

var bucketName = process.env.S3_BUCKET;
var dataDir = "/home/pdf-converter/data";
var AWS = require('aws-sdk');
AWS.config.update({accessKeyId: process.env.S3_KEY, secretAccessKey: process.env.S3_SECRET});
AWS.config.update({region: process.env.S3_REGION});

var supportedExtensions = ["docx", "doc", "odt"]
var outFileDeleted = false;

var postit = function(filename) {		
		
	var metaDataFile = filename.split(".")[0];
	var s3FilePath = fs.readFileSync(metaDataFile, 'utf8');	

	var data = fs.readFileSync(filename);
			
	console.log("upload data to S3: "+s3FilePath);
					
	var s3 = new AWS.S3();
	s3.upload({
		Bucket: bucketName,
		Key: s3FilePath,
		Body: data,
		contentType : 'application/pdf',
		ACL:'public-read'
	}, function (err,data) {
		if(err == null){
			console.log("file uploaded: " + data.Location );			
			outFileDeleted = true;
		}else{
			console.log(err);
		}
		fs.removeSync(filename);
		fs.removeSync(filename.replace(".pdf",""));
		
	});	    
}

var parser = bodyParser.urlencoded({ extended: false });    
app.post("/api", parser, function (req, res) {	
    var remotePath = url.parse(req.body.url).pathname;
	
	if(!isExtensionSupported(remotePath,supportedExtensions)){
		console.log("Extension not supported");
		res.end('{"status":0, "error":"Extension not supported"}');
		return;
	}
	var dataDir = "/home/pdf-converter/data";
	var tempFileName = Date.now();
    var fileToConvert = dataDir + "/in/" + tempFileName;	
	var downloadTempFile = dataDir + "/download/"+tempFileName;

    fsPath.writeFile(fileToConvert, 'content', function(err){
		
      var file = fs.createWriteStream(downloadTempFile); 
      var request = https.get(req.body.url, function(response) {                 
		
		if(response.statusCode != 200){ 
			console.log("Error. Status code: " + response.statusCode);
			res.end('{"status":0, "error":"Status code: '+response.statusCode+'"}'); 
			return;
		}

        response.pipe(file);
        response.on('end', function(err) {			
          var source = fs.createReadStream(downloadTempFile);
          var dest = fs.createWriteStream(fileToConvert);
          source.pipe(dest);

          fs.unlink(downloadTempFile);          		  
          
		  var outDirectory = dataDir+"/out/";
		  
          var convertFileCommand = 'bash /usr/bin/soffice --headless --convert-to pdf:writer_pdf_Export --outdir ' + outDirectory + ' ' + fileToConvert
          exec(convertFileCommand,function(error,stdout,stderr){
			  if(error == null){
				  
				  console.log("File converted succesfully to PDF: " + tempFileName+".pdf");	
				  var s3filepath = remotePath.replace("/"+bucketName+"/",""); //Remove bucket name from path
				  var extension = s3filepath.split(".").pop();	//Original file extension
				  s3filepath = s3filepath.replace(extension,"pdf");
				  
				  fs.writeFileSync(outDirectory+tempFileName, s3filepath);				  
				  res.end('{"status" : 1}');
				  fs.removeSync(fileToConvert);	
				  postit(outDirectory+tempFileName+".pdf");  //Trigger upload
				  
			  }else{
				  console.log(error);
			  }
		  });
          
        });            
    });
    }.bind({filename : req.body.filename}));
});
 
app.listen(app.get('port'), function(){
        console.log('Server listening on port: ', app.get('port'));
});

function isExtensionSupported(filename, supportedExtensions){
	var fileExtension = filename.split(".").pop();
	var isSupported = false;
	for(i=0;i<supportedExtensions.length;i++){
		if(supportedExtensions[i].includes(fileExtension)){
			isSupported = true;
			break;
		}
	}
	return isSupported;
}